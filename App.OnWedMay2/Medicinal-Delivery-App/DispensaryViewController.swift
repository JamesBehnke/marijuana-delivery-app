//
//  ViewController.swift
//  Medicinal-Delivery-App
//
//  Created by James Behnke on 2018-03-21.
//  Copyright © 2018 James Behnke. All rights reserved.
//

import UIKit

class DispensaryViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //MARK: Properties
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBAction func continueBTN(_ sender: UIButton) {
    }
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var photoImageView: UIImageView!
   
    @IBOutlet weak var ratingControl: RatingControl!
    

  
    
    override func viewDidLoad() {
        super.viewDidLoad()
      assignbackground()
        // Do any additional setup after loading the view, typically from a nib.
        self.nameTextField.delegate = self
    // Handle the text field’s user input through delegate callbacks.
       // nameTextField.delegate = self
    }
    func assignbackground(){
        let background = UIImage(named: "crystal-background.png")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       self.view.endEditing(true)
        return false
    }
    func textFieldDidEndEditing(_ textField: UITextField){
       nameLabel.text = nameTextField.text

    }
    //MARK UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        // Dismiss the picker if the user canceled.
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        // The info dictionary may contain multiple representations of the image. You want to use the original.
        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        // Set photoImageView to display the selected image.
        photoImageView.image = selectedImage
        
        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
    }
    
    //MARK ACTIONS
    @IBAction func selectImageFromPhotoLibrary(_ sender: UITapGestureRecognizer) {
        // Hide the keyboard.
        nameTextField.resignFirstResponder()
        // UIImagePickerController is a view controller that lets a user pick media from their photo library.
        let imagePickerController = UIImagePickerController()
        // Only allow photos to be picked, not taken.
        imagePickerController.sourceType = .photoLibrary
            // Make sure ViewController is notified when the user picks an image.
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    @IBAction func setCustomerNameText(_ sender: UIButton) {
        let customerName = nameTextField.text
        let hello = "Welcome, "
        nameLabel.text = hello + customerName!
    }
    @IBAction func setDefaultLabelText(_ sender: UIButton) {
        
    }
}

