//
//  Medicinal_Delivery_AppTests.swift
//  Medicinal-Delivery-AppTests
//
//  Created by James Behnke on 2018-03-21.
//  Copyright © 2018 James Behnke. All rights reserved.
//

import XCTest
@testable import Medicinal_Delivery_App

class Medicinal_Delivery_AppTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    //MARK: Meal Class Tests
    //Confirm that the dispensary initializer returns a Meal object when passed valid parameters
    func testDispensaryInitializationSucceeds(){
        // Zero rating
        let zeroRatingDispensary = Dispensary.init(name: "Zero", photo: nil, rating: 0)
        XCTAssertNotNil(zeroRatingDispensary)
        
        // Highest positive rating
        let positiveRatingDispensary = Dispensary.init(name: "Positive", photo: nil, rating: 5)
        XCTAssertNotNil(positiveRatingDispensary)
    }
    // Confirm that the Dispensary initialier returns nil when passed a negative rating or an empty name.
    func testDispensaryInitializationFails() {
        // Negative rating
        let negativeRatingDispensary = Dispensary.init(name: "Negative", photo: nil, rating: -1)
        XCTAssertNil(negativeRatingDispensary)
//        // Rating exceeds maximum
//        let largeRatingDispensary = Dispensary.init(name: "Large", photo: nil, rating: 6)
//        XCTAssertNil(largeRatingDispensary)
        // Empty String
        let emptyStringDispensary = Dispensary.init(name: "", photo: nil, rating: 0)
        XCTAssertNil(emptyStringDispensary)
    }
}
