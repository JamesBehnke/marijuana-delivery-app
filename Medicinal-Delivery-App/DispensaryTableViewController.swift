//
//  DispensaryTableViewController.swift
//  Medicinal-Delivery-App
//
//  Created by James Behnke on 2018-03-27.
//  Copyright © 2018 James Behnke. All rights reserved.
//

import UIKit

class DispensaryTableViewController: UITableViewController {
    
    //MARK: Properties
    
    var dispensaries = [Dispensary]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load the sample data.
        loadSampleDispensaries()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dispensaries.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "DispensaryTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DispensaryTableViewCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        // Fetches the appropriate meal for the data source layout.
        let dispensaryAtIndex = dispensaries[indexPath.row]
        
        cell.nameLabel.text = dispensaryAtIndex.name
        cell.photoImageView.image = dispensaryAtIndex.photo
        cell.ratingControl.rating = dispensaryAtIndex.rating
        
        return cell
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     //MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: Private Methods
    
    private func loadSampleDispensaries() {
        
        let photo1 = UIImage(named: "beKind")
        let photo2 = UIImage(named: "BGHS")
        let photo3 = UIImage(named: "OCT")
        let photo4 = UIImage(named: "TLC")
        
        guard let dispensary1 = Dispensary(name: "Be Kind", photo: photo1, rating: 4) else {
            fatalError("Unable to instantiate meal1")
        }
        
        guard let dispensary2 = Dispensary(name: "BGHS", photo: photo2, rating: 5) else {
            fatalError("Unable to instantiate meal2")
        }
        
        guard let dispensary3 = Dispensary(name: "OCT", photo: photo3, rating: 3) else {
            fatalError("Unable to instantiate meal3")
        }
        guard let dispensary4 = Dispensary(name: "TLC", photo: photo4, rating: 5) else {
            fatalError("Unable to instantiate meal4")
        }
        dispensaries += [dispensary1, dispensary2, dispensary3, dispensary4]
    }
    
}
