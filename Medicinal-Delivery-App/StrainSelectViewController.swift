//
//  StrainSelecViewController.swift
//  Medicinal-Delivery-App
//
//  Created by James Behnke on 2018-04-01.
//  Copyright © 2018 James Behnke. All rights reserved.
//

import UIKit
import os.log
class StrainSelectViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
   
    
    
    
    //RangeSlider
    //let rangeSlider = RangeSlider(frame: CGRect.zero) //rangslider
    
    
    //MARK: Properties
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var strainNameLabel: UILabel!
    
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var ratingControl: RatingControl!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var priceTextField: UITextField!
    
    var strain: Strain?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //randeslider
//        rangeSlider.backgroundColor = UIColor.red
//        view.addSubview(rangeSlider)
        //end of
        
        // Do any additional setup after loading the view.
        
        //Handle the text field's user input through delegate callbacks.
        nameTextField.delegate = self
        //Set up views if editing an existing strain.
        if let strain = strain {
            navigationItem.title = strain.name
            nameTextField.text = strain.name
            photoImageView.image = strain.photo
            ratingControl.rating = strain.rating
            //still need to add price.
        }
       // priceTextField.delegate = self
        
        //Enable the Save button only if text field has valid name
        updateSaveButtonState()
    }
    
    //rangeslider
//    override func viewDidLayoutSubviews() {
//        let margin: CGFloat = 20.0
//        let width = view.bounds.width - 2.0 * margin
//       // let yMargin = margin + view.safeAreaLayoutGuide.topAnchor.l
//        rangeSlider.frame = CGRect(x: margin, y: margin + 10, width: width, height: 31.0)
//
//    }
    
    
    
    
    
    //MARK: UITextField Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    func priceFieldDidEndEditing(_ textField: UITextField){
            priceLabel.text = textField.text
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        strainNameLabel.text = textField.text //maybe delete this line
        updateSaveButtonState()
        navigationItem.title = textField.text
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //Diable the save button when editing
        saveButton.isEnabled = false
    }
    //MARK: UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        // Dismiss the picker if the user canceled.
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        // The info dictionary may contain multiple representations of the image. You want to use the original.
        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        // Set photoImageView to display the selected image.
        photoImageView.image = selectedImage
        
        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
    }
    //MARK: Navigation
    
    //@IBOutlet weak var cancel: UIBarButtonItem!
    
    @IBAction func cancel(_ sender: UIBarButtonItem){
        dismiss(animated: true, completion: nil)
    }
    //This method allows configuration of a view controller before its presented.
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
       super.prepare(for: segue, sender: sender)
        
        //Configure the destination view controller only when the save button is pressed.
        guard let button = sender as? UIBarButtonItem, button === saveButton else{
            os_log("The save button was not pressed, cacelling", log: OSLog.default, type: .debug)
            return
        }
        let name = nameTextField.text ?? ""
        let photo = photoImageView.image
        let rating = ratingControl.rating
        let price = priceTextField.text ?? ""
        //Set strain to be passed to StrainTableViewCell
        strain = Strain(name: name, photo: photo, rating: rating, price: price)
        
    }
    
    
    //MARK: Actions

    @IBAction func selectImageFromLibrary(_ sender: UITapGestureRecognizer) {
        // Hide the keyboard.
        nameTextField.resignFirstResponder()
        
        // UIImagePickerController is a view controller that lets a user pick media from their photo library.
        let imagePickerController = UIImagePickerController()
        
        // Only allow photos to be picked, not taken.
        imagePickerController.sourceType = .photoLibrary
        
        // Make sure ViewController is notified when the user picks an image.
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func setDefaultLabelText(_ sender: UIButton) {
        strainNameLabel.text = "Default text"
    }
    
    //MARK: Private Methods
    private func updateSaveButtonState(){
        // Disable the save button if the text field is empty
        let text = nameTextField.text ?? ""
        saveButton.isEnabled = !text.isEmpty
    }

//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
