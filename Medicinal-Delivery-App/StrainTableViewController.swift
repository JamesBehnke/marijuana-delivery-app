//
//  StrainTableViewController.swift
//  Medicinal-Delivery-App
//
//  Created by James Behnke on 2018-03-31.
//  Copyright © 2018 James Behnke. All rights reserved.
//

import UIKit
import os.log


class StrainTableViewController: UITableViewController {
    //MARK:Properties
    
    //MARK: Properties
    
    var strains = [Strain]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load the sample data.
        loadSampleStrains()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return strains.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "StrainTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? StrainTableViewCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        // Fetches the appropriate meal for the data source layout.
        let strainAtIndex = strains[indexPath.row]
        
      cell.nameLabel.text = strainAtIndex.name
        cell.photoImageView.image = strainAtIndex.photo
        cell.ratingControl.rating = strainAtIndex.rating
        cell.priceLabel.text = strainAtIndex.price
        
        return cell
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
     //MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        switch(segue.identifier ?? ""){
        
        case "AddItem":
            os_log("Adding a new Strain.", log:OSLog.default, type: .debug)
        
        case "ShowDetail":
            guard let strainDetailViewController = segue.destination as? StrainSelectViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let selectedStrainCell = sender as? StrainTableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            guard let indexPath = tableView.indexPath(for: selectedStrainCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            let selectedStrain = strains[indexPath.row]
            strainDetailViewController.strain = selectedStrain
            
        default:
            fatalError("Unexpected Segue Identifier: \(String(describing: segue.identifier))")
        }
     }
 
    
    // MARK: Actions
    @IBAction func unwindToStrainList(sender: UIStoryboardSegue){
        if let sourceViewController = sender.source as? StrainSelectViewController, let strain = sourceViewController.strain{
            //Add new Strain
            let newIndexPath = IndexPath(row: strains.count, section: 0)
            
            strains.append(strain)
            tableView.insertRows(at: [newIndexPath], with: .automatic)
        }
    }
    
    //MARK: Private Methods
    
    private func loadSampleStrains() {
        
        let photo1 = UIImage(named: "SourDiesel")
        let photo2 = UIImage(named: "LemonHaze")
        let photo3 = UIImage(named: "BubbaKush")
        //let photo4 = UIImage(named: "TLC")
        
        guard let strain1 = Strain(name: "Sour Diesel", photo: photo1, rating: 4, price: "10") else {
            fatalError("Unable to instantiate meal1")
        }
        
        guard let strain2 = Strain(name: "Lemon Haze", photo: photo2, rating: 5, price: "7.5") else {
            fatalError("Unable to instantiate meal2")
        }
        
        guard let strain3 = Strain(name: "Bubba Kush", photo: photo3, rating: 5, price: "9") else {
            fatalError("Unable to instantiate meal3")
        }
//        guard let strain4 = Strain(name: "OKT", photo: photo4, rating: 5) else {
//            fatalError("Unable to instantiate meal4")
//        }
        strains += [strain1, strain2, strain3]
    }
    
}

